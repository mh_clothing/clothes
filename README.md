# README #

A repository for storing clothing for Makehuman models.

The categories are taken from http://en.wikipedia.org/wiki/Category:Clothing_by_type. Sub-categories should be
made if needed and should match the names used on that page.
